<?php  

namespace App\Entities;

use App\Entities\Product;
use App\Entities\ProductInterface;


class ProductTest extends \PHPUnit\Framework\TestCase
{
	public function testProductType()
	{
		$product = new Product();
		$this->assertInstanceOf(ProductInterface::class, $product);
	}

	public function testPriceValue()
	{
		$product = new Product();
		$product->setPrice(10);
		$this->assertEquals(10, $product->getPrice());
	}

	/**
	 * @expectedException InvalidArgumentException 
	 */
	public function testPriceValueWhenANotNumbericGiven()
	{
		$product = new Product();
		$product->setPrice('teste');
	}

}
?>