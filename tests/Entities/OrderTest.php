<?php  

namespace App\Entities;

use App\Entities\Product;
use App\Entities\ProductInterface;


class CartTest extends \PHPUnit\Framework\TestCase
{
	public function testGetProductList()
	{
		$product1 = new Product();
		$product1->setName('Product 1');
		$product1->setDescription('Desc 1');
		$product1->setPrice(10);

		$product2 = new Product();
		$product2->setName('Product 2');
		$product2->setDescription('Desc 2');
		$product2->setPrice(20);

		$product3 = new Product();
		$product3->setName('Product 3');
		$product3->setDescription('Desc 3');
		$product3->setPrice(30);

		$cart = new Cart();
		$cart->addProduct($product1);
		$cart->addProduct($product2);
		$cart->addProduct($product3);

		$total=60;
		$this->assertEquals($total, $cart->getTotal());

		/*$products = new \ArrayObject([$product1, $product2, $product3]);
		$this->assertEquals($products, $cart->getProducts());*/
	}
}
?>