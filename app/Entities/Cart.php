<?php 

namespace App\Entities;

use App\Entities\CartInterface;

class Cart implements CartInterface
{
	private $products;

	public function __construct()
	{
		$this->products = new \ArrayObject();
	}

	public function getProducts()
	{
		return $this->products;
	}

	public function addProduct(ProductInterface $product)
	{
		$this->products->append($product);
	}

	public function getTotal()
	{
		$total = 0;
		foreach($this->products as $product):
			$total += $product->getPrice();
		endforeach;
		return $total;
	}
}
?>